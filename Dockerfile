FROM openjdk:14
RUN mkdir /app
COPY ./target/metlify-0.0.1-SNAPSHOT.jar /app
WORKDIR /app
CMD ["java", "-jar", "metlify-0.0.1-SNAPSHOT.jar"]