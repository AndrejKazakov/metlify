package de.htw.metlify.database.repositories;

import de.htw.metlify.models.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void findByUsernameShouldReturnUser() {
        User peter = new User();
        peter.setUsername("peter");
        peter.setEmail("a@b.de");
        peter.setPassword("password");
        entityManager.persist(peter);
        entityManager.flush();

        User peterFromDB = userRepository.findByUsername("peter");

        assertThat(peterFromDB.getUsername()).isEqualTo(peter.getUsername());
    }
}
