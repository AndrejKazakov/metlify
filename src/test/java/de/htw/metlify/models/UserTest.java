package de.htw.metlify.models;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import javax.validation.*;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class UserTest {
    private static ValidatorFactory validatorFactory;
    private static Validator validator;

    @BeforeClass
    public static void createValidator() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @AfterClass
    public static void close() {
        validatorFactory.close();
    }

    @Test
    public void beanValidationOnUserNameShouldThrowException() {

        User user = new User(1, null, "a@b.de", "password", 23, "male",
                "Berlin", "thrash metal", "Metallica", false, null,
                null, null, null);

        Set<ConstraintViolation<User>> violations
                = validator.validate(user);

        assertEquals(violations.size(), 1);

        ConstraintViolation<User> violation
                = violations.iterator().next();

        assertEquals("must not be null",
                violation.getMessage());

        assertEquals("username", violation.getPropertyPath().toString());
    }

    @Test
    public void beanValidationOnUserNameIsTooShortShouldThrowException() {

        User user = new User(1, "a", "a@b.de", "password", 23, "male",
                "Berlin", "thrash metal", "Metallica", false, null,
                null, null, null);

        Set<ConstraintViolation<User>> violations
                = validator.validate(user);

        assertEquals(violations.size(), 1);

        ConstraintViolation<User> violation
                = violations.iterator().next();

        assertEquals("Your username should be atleast 2 Characters and maximum 40",
                violation.getMessage());

        assertEquals("username", violation.getPropertyPath().toString());
    }

    @Test
    public void beanValidationOnUserIsValid() {

        User user = new User(1, "peter", "a@b.de", "password", 23, "male",
                "Berlin", "thrash metal", "Metallica", false, null,
                null, null, null);

        Set<ConstraintViolation<User>> violations
                = validator.validate(user);

        assertEquals(violations.size(), 0);
    }

}