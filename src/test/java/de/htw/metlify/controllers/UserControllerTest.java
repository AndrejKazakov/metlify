package de.htw.metlify.controllers;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.htw.metlify.models.User;
import de.htw.metlify.services.login.LoginService;
import de.htw.metlify.services.playlist.PlaylistService;
import de.htw.metlify.services.registration.RegistrationService;
import de.htw.metlify.services.song.SongService;
import de.htw.metlify.services.user.UserService;
import de.htw.metlify.utils.JwtTokenValidator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userService;
    @MockBean
    private RegistrationService registrationService;
    @MockBean
    private SongService songService;
    @MockBean
    private PlaylistService playlistService;
    @MockBean
    private JwtTokenValidator tokenValidator;
    @MockBean
    private LoginService loginService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void registerUserShouldReturn201() throws Exception {
        User peter = new User();
        peter.setUsername("peter");
        peter.setEmail("a@b.de");
        peter.setPassword("password");

        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);

        Mockito.when(registrationService.registerUser(peter)).thenReturn(peter);

        mvc.perform(post("/users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(peter)))
                .andExpect(status().isCreated())
                .andReturn();
    }

    @Test
    public void registerUserWithWrongCredsShouldReturn400() throws Exception {
        User peter = new User();
        peter.setUsername("peter");
        peter.setEmail("a@b.de");

        objectMapper.disable(MapperFeature.USE_ANNOTATIONS);

        Mockito.when(registrationService.registerUser(peter)).thenReturn(peter);

        mvc.perform(post("/users/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(peter)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }
}
