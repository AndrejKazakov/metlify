package de.htw.metlify.services;

import de.htw.metlify.database.repositories.UserRepository;
import de.htw.metlify.models.User;
import de.htw.metlify.services.user.UserService;
import de.htw.metlify.services.user.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class UserServiceIntegrationTest {

    @TestConfiguration
    static class UserServiceTestContextConfiguration {
        @Bean
        public UserService userService() {
            return new UserServiceImpl();
        }
    }

    @MockBean
    public UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        User peter = new User();
        peter.setUsername("peter");
        peter.setEmail("a@b.de");
        peter.setPassword("password");
        peter.setPrivate(false);

        Mockito.when(userRepository.findByUsernameAndIsPrivate(peter.getUsername(), false)).thenReturn(peter);
    }

    @Test
    public void whenUserNameIsFoundShouldReturnUser() {
        String name = "peter";
        User userFromDB = userService.findUserByUsername(name);

        assertThat(userFromDB.getUsername()).isEqualTo(name);
    }
}
