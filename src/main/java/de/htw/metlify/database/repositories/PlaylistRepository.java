package de.htw.metlify.database.repositories;

import de.htw.metlify.models.Playlist;
import de.htw.metlify.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import java.util.List;

public interface PlaylistRepository  extends JpaRepository<Playlist, Integer>, JpaSpecificationExecutor<Playlist> {
    Playlist findById(int id);
    List<Playlist> findAllById(Iterable<Integer> ids);
    List<Playlist> findByTitleAndIsPrivate(String title, boolean isPrivate);
    List<Playlist> findByOwnerAndIsPrivate(User user, boolean isPrivate);
    List<Playlist> findByGenreAndIsPrivate(String genre, boolean isPrivate);
    List<Playlist> findAllByIsPrivate(boolean isPrivate);
}
