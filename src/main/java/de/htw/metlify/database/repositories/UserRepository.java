package de.htw.metlify.database.repositories;

import de.htw.metlify.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {
    User findById(int id);
    User findByIdAndIsPrivate(int id, boolean isPrivate);
    User findByUsername(String username);
    User findByUsernameAndIsPrivate(String username, boolean isPrivate);
    User findByEmail(String email);
    User findByEmailAndIsPrivate(String email, boolean isPrivate);
    List<User> findAllByIsPrivate(boolean isPrivate);
    List<User> findByLocationAndIsPrivate(String location, boolean isPrivate);
    List<User> findByFavouriteGenreAndIsPrivate(String favouriteGenre, boolean isPrivate);
    List<User> findByFavouriteBandAndIsPrivate(String favouriteBand, boolean isPrivate);
}
