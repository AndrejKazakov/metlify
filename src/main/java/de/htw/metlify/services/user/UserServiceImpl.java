package de.htw.metlify.services.user;

import de.htw.metlify.database.repositories.UserRepository;
import de.htw.metlify.exceptions.UserEmailAlreadyInUseException;
import de.htw.metlify.exceptions.UsernameAlreadyInUseExcection;
import de.htw.metlify.models.Playlist;
import de.htw.metlify.models.Song;
import de.htw.metlify.models.User;
import de.htw.metlify.models.requests.updates.user.*;
import de.htw.metlify.database.specifications.UserSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    private final boolean IS_NOT_PRIVATE = false;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserServiceImpl(){}

    @Override
    public User findUserById(Integer id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isEmpty() || userOptional.get().isPrivate()) {
            return null;
        } else {
            return userOptional.get();
        }
    }

    @Override
    public List<User> findAllUsers() {
        return userRepository.findAllByIsPrivate(IS_NOT_PRIVATE);
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findByUsernameAndIsPrivate(username, IS_NOT_PRIVATE);
    }

    @Override
    public boolean isEmailAlreadyInUse(String email) {
        return userRepository.findByEmail(email) != null;
    }

    @Override
    public boolean isUsernameAlreadyInUse(String username) {
        return userRepository.findByUsername(username) != null;
    }

    @Override
    public List<User> getUserByParameter(String parameter, String value) {
        UserSpecification us = new UserSpecification(parameter, value);
        List<User> users = userRepository.findAll(us);
        List<User> publicUsers = filterOutPrivateUsers(users);

        return publicUsers;
    }
    private List<User> filterOutPrivateUsers(List<User> users) {
        return users.stream().filter(user -> !user.isPrivate()).collect(Collectors.toList());
    }

    @Override
    public User updateUsername(UpdateUsernameRequest request) throws UsernameAlreadyInUseExcection{
        String newUsername = request.getNewUsername();
        User userFromDB = userRepository.findByIdAndIsPrivate(request.getId(), false);

        if (!isUsernameAlreadyInUse(newUsername)) {
            userFromDB.setUsername(newUsername);

            return userRepository.save(userFromDB);
        } else {
            throw new UsernameAlreadyInUseExcection();
        }
    }

    @Override
    public User updateEmail(UpdateEmailRequest request) throws UserEmailAlreadyInUseException {
        String newEmail = request.getNewEmail();
        User userFromDB = userRepository.findById(request.getId());

        if (!isEmailAlreadyInUse(newEmail)) {
            userFromDB.setEmail(newEmail);

            return userRepository.save(userFromDB);
        } else {
            throw new UserEmailAlreadyInUseException();
        }
    }

    @Override
    public User updateSex(UpdateSexRequest request) {
        String newSex = request.getNewSex();
        User userFromDB = userRepository.findById(request.getId());
        userFromDB.setSex(newSex);

        return userRepository.save(userFromDB);
    }

    @Override
    public User updateLocation(UpdateLocationRequest request) {
        String newLocation = request.getNewLocation();
        User userFromDB = userRepository.findById(request.getId());
        userFromDB.setLocation(newLocation);

        return userRepository.save(userFromDB);
    }

    @Override
    public User updateFavouriteGenre(UpdateFavouriteGenreRequest request) {
        String newFavouriteGenre = request.getNewFavouriteGenre();
        User userFromDB = userRepository.findById(request.getId());
        userFromDB.setFavouriteGenre(newFavouriteGenre);

        return userRepository.save(userFromDB);
    }

    @Override
    public User updateFavouriteBand(UpdateFavouriteBandRequest request) {
        String newFavouriteBand = request.getNewFavouriteBand();
        User userFromDB = userRepository.findById(request.getId());
        userFromDB.setFavouriteBand(newFavouriteBand);

        return userRepository.save(userFromDB);
    }

    @Override
    public User updateIsPrivate(UpdateIsPrivateRequest request) {
        boolean isPrivate = request.isPrivate();
        User userFromDB = userRepository.findById(request.getId());
        userFromDB.setPrivate(isPrivate);

        return userRepository.save(userFromDB);
    }

    @Override
    public User updateLikedSongs(int userId, Set<Song> newLikedSongs) {
        User userFromDB = userRepository.findById(userId);
        userFromDB.setLikedSongs(newLikedSongs);

        return userRepository.save(userFromDB);
    }

    @Override
    public User updateLikedPlaylists(int userId, Set<Playlist> newLikedPlaylists) {
        User userFromDB = userRepository.findById(userId);
        userFromDB.setLikedPlaylists(newLikedPlaylists);

        return userRepository.save(userFromDB);
    }

    @Override
    public void deleteUser(Integer id) {
        userRepository.deleteById(id);
    }
}
