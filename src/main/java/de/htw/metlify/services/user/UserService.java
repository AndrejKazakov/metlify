package de.htw.metlify.services.user;

import de.htw.metlify.exceptions.UserEmailAlreadyInUseException;
import de.htw.metlify.exceptions.UsernameAlreadyInUseExcection;
import de.htw.metlify.models.Playlist;
import de.htw.metlify.models.Song;
import de.htw.metlify.models.User;
import de.htw.metlify.models.requests.updates.user.*;
import java.util.List;
import java.util.Set;

public interface UserService {
    User findUserById(Integer id);
    List<User> findAllUsers();
    User findUserByUsername(String username);
    User updateUsername(UpdateUsernameRequest request) throws UsernameAlreadyInUseExcection;
    User updateEmail(UpdateEmailRequest request) throws UserEmailAlreadyInUseException;
    User updateSex(UpdateSexRequest request);
    User updateLocation(UpdateLocationRequest request);
    User updateFavouriteGenre(UpdateFavouriteGenreRequest request);
    User updateFavouriteBand(UpdateFavouriteBandRequest request);
    User updateIsPrivate(UpdateIsPrivateRequest request);
    User updateLikedSongs(int userId, Set<Song> newLikedSongs);
    User updateLikedPlaylists(int userId, Set<Playlist> newLikedPlaylists);
    boolean isEmailAlreadyInUse(String value);
    boolean isUsernameAlreadyInUse(String value);
    List<User> getUserByParameter(String parameter, String value);
    void deleteUser(Integer id);
}
