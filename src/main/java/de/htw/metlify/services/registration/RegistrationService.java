package de.htw.metlify.services.registration;

import de.htw.metlify.models.requests.updates.user.NewPasswordRequest;
import de.htw.metlify.models.User;

public interface RegistrationService {
    User registerUser(User user);
    User updateUserPassword(NewPasswordRequest request);
}
