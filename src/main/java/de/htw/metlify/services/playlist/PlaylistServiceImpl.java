package de.htw.metlify.services.playlist;

import de.htw.metlify.database.repositories.PlaylistRepository;
import de.htw.metlify.database.specifications.PlaylistSpecification;
import de.htw.metlify.models.Playlist;
import de.htw.metlify.models.Song;
import de.htw.metlify.models.User;
import de.htw.metlify.models.requests.updates.playlist.UpdatePlaylistGenreRequest;
import de.htw.metlify.models.requests.updates.playlist.UpdatePlaylistIsPrivateRequest;
import de.htw.metlify.models.requests.updates.playlist.UpdatePlaylistTitleRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PlaylistServiceImpl implements PlaylistService {

    private final PlaylistRepository playlistRepository;
    private final boolean IS_NOT_PRIVATE = false;
    private final String DATE_FORMAT = "dd.MM.yyyy HH:mm:ss";

    @Autowired
    public PlaylistServiceImpl(PlaylistRepository songRepository) {
        this.playlistRepository = songRepository;
    }


    @Override
    public List<Playlist> findAllPlaylists() {
        return playlistRepository.findAllByIsPrivate(IS_NOT_PRIVATE);
    }


    @Override
    public List<Playlist> getPlaylistsByParameter(String parameter, String value) {
        PlaylistSpecification playlistSpecification = new PlaylistSpecification(parameter, value);
        List<Playlist> playlists = playlistRepository.findAll(playlistSpecification);
        List<Playlist> publicPlaylists = filterOutPrivatePlaylists(playlists);

        return publicPlaylists;
    }

    private List<Playlist> filterOutPrivatePlaylists(List<Playlist> playlists) {
        return playlists.stream().filter(song -> !song.isPrivate()).collect(Collectors.toList());
    }

    @Override
    public Playlist createPlaylist(Playlist playlist) {
        String updatedAt = getCurrentDateString();
        playlist.setLastUpdated(updatedAt);
        return playlistRepository.save(playlist);
    }


    @Override
    public Playlist updateTitle(UpdatePlaylistTitleRequest request) {
        String newTitle = request.getNewTitle();
        Playlist playlistFromDB = playlistRepository.findById(request.getId());
        playlistFromDB.setTitle(newTitle);

        String updatedAt = getCurrentDateString();
        playlistFromDB.setLastUpdated(updatedAt);

        return playlistRepository.save(playlistFromDB);
    }

    @Override
    public Playlist updateGenre(UpdatePlaylistGenreRequest request) {
        String newGenre = request.getNewGenre();
        Playlist playlistFromDB = playlistRepository.findById(request.getId());
        playlistFromDB.setGenre(newGenre);

        String updatedAt = getCurrentDateString();
        playlistFromDB.setLastUpdated(updatedAt);

        return playlistRepository.save(playlistFromDB);
    }

    @Override
    public Playlist updateSongs(int id, Set<Song> newSongs) {
        Playlist playlistFromDB = playlistRepository.findById(id);
        playlistFromDB.setSongs(newSongs);

        String updatedAt = getCurrentDateString();
        playlistFromDB.setLastUpdated(updatedAt);

        return playlistRepository.save(playlistFromDB);
    }

    @Override
    public Playlist updateIsPrivate(UpdatePlaylistIsPrivateRequest request) {
        boolean isPrivate = request.isPrivate();
        Playlist playlistFromDB = playlistRepository.findById(request.getId());
        playlistFromDB.setPrivate(isPrivate);

        String updatedAt = getCurrentDateString();
        playlistFromDB.setLastUpdated(updatedAt);

        return playlistRepository.save(playlistFromDB);
    }

    @Override
    public void deletePlaylist(Integer id) {
        playlistRepository.deleteById(id);
    }

    private String getCurrentDateString() {
        return new SimpleDateFormat(DATE_FORMAT).format(new Date());
    }

    @Override
    public Playlist findPlaylistById(Integer id) {
        Optional<Playlist> playlistOptional = playlistRepository.findById(id);
        if (playlistOptional.isEmpty()) {
            return null;
        } else {
            return playlistOptional.get();
        }
    }

    @Override
    public List<Playlist> findPlaylistsByOwner(User user) {
        return playlistRepository.findByOwnerAndIsPrivate(user, IS_NOT_PRIVATE);
    }

    @Override
    public boolean arePlaylistsInDatabase(Set<Integer> playListIds) {
        for (int id : playListIds) {
            if (!playlistRepository.existsById(id)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Set<Playlist> getAllPlaylistsWithMultipleIds(Set<Integer> playlistIds) {
        List<Playlist> playlistList = playlistRepository.findAllById(playlistIds);
        return new HashSet<>(playlistList);
    }
}
