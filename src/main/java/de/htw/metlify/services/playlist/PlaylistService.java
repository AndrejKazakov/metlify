package de.htw.metlify.services.playlist;

import de.htw.metlify.models.Playlist;
import de.htw.metlify.models.Song;
import de.htw.metlify.models.User;
import de.htw.metlify.models.requests.updates.playlist.UpdatePlaylistGenreRequest;
import de.htw.metlify.models.requests.updates.playlist.UpdatePlaylistIsPrivateRequest;
import de.htw.metlify.models.requests.updates.playlist.UpdatePlaylistTitleRequest;
import java.util.List;
import java.util.Set;

public interface PlaylistService {
    boolean arePlaylistsInDatabase(Set<Integer> playListIds);
    Set<Playlist> getAllPlaylistsWithMultipleIds(Set<Integer> newLikedPlaylistIds);
    Playlist findPlaylistById(Integer id);
    List<Playlist> findPlaylistsByOwner(User user);
    List<Playlist> findAllPlaylists();
    Playlist createPlaylist(Playlist playlist);
    Playlist updateTitle(UpdatePlaylistTitleRequest request);
    Playlist updateGenre(UpdatePlaylistGenreRequest request);
    Playlist updateSongs(int id, Set<Song> newSongs);
    Playlist updateIsPrivate(UpdatePlaylistIsPrivateRequest request);
    List<Playlist> getPlaylistsByParameter(String parameter, String value);
    void deletePlaylist(Integer id);
}
