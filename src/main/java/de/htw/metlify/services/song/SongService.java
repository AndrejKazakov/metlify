package de.htw.metlify.services.song;

import de.htw.metlify.models.Song;
import de.htw.metlify.models.User;
import de.htw.metlify.models.requests.updates.song.*;
import java.util.List;
import java.util.Set;

public interface SongService {
    boolean isSongInDatabase(Integer id);
    boolean areSongsInDatabase(Set<Integer> songIds);
    Set<Song> getAllSongsWithMultipleIds(Set<Integer> songIds);
    Song findSongById(Integer id);
    List<Song> findSongsByArtist(User artist);
    List<Song> findSongsByReleased(Integer released);
    List<Song> findSongsByIsExplicit(boolean isExplicit);
    Song createSong(Song song);
    Song updateTitle(UpdateSongTitleRequest request);
    Song updateLabel(UpdateSongLabelRequest request);
    Song updateIsExplicit(UpdateSongIsExplicitRequest request);
    Song updateLength(UpdateSongLengthRequest request);
    Song updateAlbum(UpdateSongAlbumRequest request);
    Song updateIsPrivate(UpdateSongIsPrivateRequest request);
    Song updateGenre(UpdateSongGenreRequest request);
    List<Song> getSongsByParameter(String parameter, String value);
    List<Song> getRandomSongsByGenre(String genre);
    void deleteSong(Integer id);
}
