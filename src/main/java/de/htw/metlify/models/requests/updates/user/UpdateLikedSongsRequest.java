package de.htw.metlify.models.requests.updates.user;

import de.htw.metlify.models.Song;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Set;

public class UpdateLikedSongsRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    private Set<Integer> newLikedSongsIds;

    public UpdateLikedSongsRequest(int id, Set<Integer> likedSongsIds) {
        this.id = id;
        this.newLikedSongsIds = likedSongsIds;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Integer> getNewLikedSongsIds() {
        return newLikedSongsIds;
    }

    public void setNewLikedSongsIds(Set<Integer> newLikedSongsIds) {
        this.newLikedSongsIds = newLikedSongsIds;
    }
}
