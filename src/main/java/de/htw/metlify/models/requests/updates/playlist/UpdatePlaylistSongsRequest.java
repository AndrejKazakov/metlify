package de.htw.metlify.models.requests.updates.playlist;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Set;

public class UpdatePlaylistSongsRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    private Set<Integer> newSongs;

    public UpdatePlaylistSongsRequest(int id, Set<Integer> newSongs) {
        this.id = id;
        this.newSongs = newSongs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Integer> getNewSongs() {
        return newSongs;
    }

    public void setNewSongs(Set<Integer> newSongs) {
        this.newSongs = newSongs;
    }
}
