package de.htw.metlify.models.requests.updates.playlist;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class UpdatePlaylistIsPrivateRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    private boolean isPrivate;

    public UpdatePlaylistIsPrivateRequest(int id, boolean isPrivate) {
        this.id = id;
        this.isPrivate = isPrivate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }
}
