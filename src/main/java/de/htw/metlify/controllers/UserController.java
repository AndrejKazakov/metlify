package de.htw.metlify.controllers;

import de.htw.metlify.exceptions.UserEmailAlreadyInUseException;
import de.htw.metlify.exceptions.UsernameAlreadyInUseExcection;
import de.htw.metlify.models.Playlist;
import de.htw.metlify.models.Song;
import de.htw.metlify.models.User;
import de.htw.metlify.models.requests.updates.user.*;
import de.htw.metlify.services.playlist.PlaylistService;
import de.htw.metlify.services.song.SongService;
import de.htw.metlify.utils.JwtTokenValidator;
import de.htw.metlify.services.registration.RegistrationService;
import de.htw.metlify.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final SongService songService;
    private final PlaylistService playlistService;
    private final JwtTokenValidator tokenValidator;
    private final RegistrationService registrationService;

    @Autowired
    public UserController(RegistrationService registrationService, UserService userService, SongService songService,
                          PlaylistService playlistService, JwtTokenValidator tokenValidator) {
        this.registrationService = registrationService;
        this.userService = userService;
        this.songService = songService;
        this.playlistService = playlistService;
        this.tokenValidator = tokenValidator;
    }

    @GetMapping()
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userService.findAllUsers();
        if (users.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(users);
        }
    }

    @GetMapping("/getBy")
    public ResponseEntity<List<User>> getUsersByParameter(@RequestParam(name = "parameter") String parameter, @RequestParam(name = "value") String value) {
        try {
            List<User> userList = userService.getUserByParameter(parameter, value);

            if (userList.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            } else {
                return ResponseEntity.status(HttpStatus.OK).body(userList);
            }
        } catch (InvalidDataAccessApiUsageException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") Integer id) {
        User user = userService.findUserById(id);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(user);
        }
    }

    @PostMapping("/register")
    public ResponseEntity<User> registerUser(@Valid @RequestBody User user) {
        User registeredUser = registrationService.registerUser(user);

        return ResponseEntity.status(HttpStatus.CREATED).body(registeredUser);
    }

    @PutMapping("/username")
    public ResponseEntity<User> updateUsername(@Valid @RequestBody UpdateUsernameRequest request,
                                               @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorized(request.getId(), authHeader)) {
                User updatedUser = userService.updateUsername(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (UsernameAlreadyInUseExcection e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }

    @PutMapping("/email")
    public ResponseEntity<User> updateUserEmail(@Valid @RequestBody UpdateEmailRequest request,
                                                @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorized(request.getId(), authHeader)) {
                User updatedUser = userService.updateEmail(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (UserEmailAlreadyInUseException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }

    @PutMapping("/sex")
    public ResponseEntity<User> updateUserSex(@Valid @RequestBody UpdateSexRequest request,
                                              @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(request.getId(), authHeader)) {
            User updatedUser = userService.updateSex(request);

            return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PutMapping("/location")
    public ResponseEntity<User> updateUserLocation(@Valid @RequestBody UpdateLocationRequest request,
                                                   @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(request.getId(), authHeader)) {
            User updatedUser = userService.updateLocation(request);

            return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PutMapping("/favouriteGenre")
    public ResponseEntity<User> updateUserFavouriteGenre(@Valid @RequestBody UpdateFavouriteGenreRequest request,
                                                         @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(request.getId(), authHeader)) {
            User updatedUser = userService.updateFavouriteGenre(request);

            return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PutMapping("/favouriteBand")
    public ResponseEntity<User> updateUserFavouriteBand(@Valid @RequestBody UpdateFavouriteBandRequest request,
                                                        @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(request.getId(), authHeader)) {
            User updatedUser = userService.updateFavouriteBand(request);

            return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PutMapping("/isPrivate")
    public ResponseEntity<User> updateUserIsPrivate(@Valid @RequestBody UpdateIsPrivateRequest request,
                                                    @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(request.getId(), authHeader)) {
            User updatedUser = userService.updateIsPrivate(request);

            return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PutMapping("/likedSongs")
    public ResponseEntity<User> updateUserLikedSongs(@Valid @RequestBody UpdateLikedSongsRequest request,
                                                     @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        int userId = request.getId();
        if (userIsAuthorized(userId, authHeader)) {
            if (songsAreInDatabase(request.getNewLikedSongsIds())) {
                Set<Song> newLikedSongs = songService.getAllSongsWithMultipleIds(request.getNewLikedSongsIds());
                User updatedUser = userService.updateLikedSongs(userId, newLikedSongs);
                return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    private boolean songsAreInDatabase(Set<Integer> songIds) {
        return songService.areSongsInDatabase(songIds);
    }

    @PutMapping("/likedPlaylists")
    public ResponseEntity<User> updateUserLikedPlaylists(@Valid @RequestBody UpdateLikedPlaylistsRequest request,
                                                         @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        int userId = request.getId();
        if (userIsAuthorized(userId, authHeader)) {
            if (playListsAreInDatabase(request.getNewLikedPlaylistIds())) {
                Set<Playlist> newLikedPlaylists = playlistService.getAllPlaylistsWithMultipleIds(request.getNewLikedPlaylistIds());
                User updatedUser = userService.updateLikedPlaylists(userId, newLikedPlaylists);
                return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    private boolean playListsAreInDatabase(Set<Integer> playListIds) {
        return playlistService.arePlaylistsInDatabase(playListIds);
    }

    @PutMapping("/password")
    public ResponseEntity<User> updateUserPassword(@Valid @RequestBody NewPasswordRequest request,
                                                   @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(request.getId(), authHeader)) {
            registrationService.updateUserPassword(request);

            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<User> deleteUserById(@PathVariable("id") Integer id,
                                               @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(id, authHeader)) {
            userService.deleteUser(id);

            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    private boolean userIsAuthorized(int id, String authHeader) {
        return id == tokenValidator.getIdFromAuthHeader(authHeader);
    }

    // This is needed for elegant error message when bean validation fails on registration
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleError(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return errors;
    }

}
