package de.htw.metlify.controllers;

import de.htw.metlify.models.Song;
import de.htw.metlify.models.User;
import de.htw.metlify.models.requests.updates.song.*;
import de.htw.metlify.services.song.SongService;
import de.htw.metlify.services.user.UserService;
import de.htw.metlify.utils.JwtTokenValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/songs")
public class SongController {

    private final SongService songService;
    private final JwtTokenValidator tokenValidator;
    private final UserService userService;

    @Autowired
    public SongController(SongService songService, JwtTokenValidator tokenValidator, UserService userService) {
        this.songService = songService;
        this.tokenValidator = tokenValidator;
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Song> getSongById(@PathVariable("id") Integer id,
                                            @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        Song song = songService.findSongById(id);

        if (song == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else if (!song.isPrivate() || userIsAuthorized(song.getArtist().getId(), authHeader)) {
            return ResponseEntity.status(HttpStatus.OK).body(song);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    private boolean userIsAuthorized(int id, String authHeader) {
        return id == tokenValidator.getIdFromAuthHeader(authHeader);
    }

    @GetMapping("/getBy")
    public ResponseEntity<List<Song>> getSongsByParameter(@RequestParam(name = "parameter") String parameter, @RequestParam(name = "value") String value) {
        try {
            List<Song> songList = songService.getSongsByParameter(parameter, value);

            if (songList.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            } else {
                return ResponseEntity.status(HttpStatus.OK).body(songList);
            }
        } catch (InvalidDataAccessApiUsageException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping("/artist")
    public ResponseEntity<List<Song>> getSongsByArtist(@RequestParam(name = "artist") String artistName) {
        User user = userService.findUserByUsername(artistName);

        List<Song> songs = songService.findSongsByArtist(user);

        if (songs.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(songs);
        }
    }

    @GetMapping("/released")
    public ResponseEntity<List<Song>> getSongsByReleased(@RequestParam(name = "released") Integer released) {
        List<Song> songs = songService.findSongsByReleased(released);

        if (songs.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(songs);
        }
    }

    @GetMapping("/isExplicit")
    public ResponseEntity<List<Song>> getSongsByIsExplicit(@RequestParam(name = "isExplicit") boolean isExplicit) {
        List<Song> songs = songService.findSongsByIsExplicit(isExplicit);

        if (songs.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(songs);
        }
    }

    @PostMapping
    public ResponseEntity<Song> createNewSong(@Valid @RequestBody Song song,
                                              @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        User artist = userService.findUserById(tokenValidator.getIdFromAuthHeader(authHeader));
        song.setArtist(artist);
        Song addedSong = songService.createSong(song);

        return ResponseEntity.status(HttpStatus.OK).body(addedSong);
    }

    @PutMapping("/title")
    public ResponseEntity<Song> updateSongTitle(@Valid @RequestBody UpdateSongTitleRequest request,
                                                @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader)) {
                Song updatedSong = songService.updateTitle(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedSong);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }


    @PutMapping("/label")
    public ResponseEntity<Song> updateSongLabel(@Valid @RequestBody UpdateSongLabelRequest request,
                                                @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader)) {
                Song updatedSong = songService.updateLabel(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedSong);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/genre")
    public ResponseEntity<Song> updateSongGenre(@Valid @RequestBody UpdateSongGenreRequest request,
                                                @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader)) {
                Song updatedSong = songService.updateGenre(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedSong);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/isExplicit")
    public ResponseEntity<Song> updateSongIsExplicit(@Valid @RequestBody UpdateSongIsExplicitRequest request,
                                                     @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader)) {
                Song updatedSong = songService.updateIsExplicit(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedSong);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/length")
    public ResponseEntity<Song> updateSongLength(@Valid @RequestBody UpdateSongLengthRequest request,
                                                 @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader)) {
                Song updatedSong = songService.updateLength(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedSong);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/album")
    public ResponseEntity<Song> updateSongLength(@Valid @RequestBody UpdateSongAlbumRequest request,
                                                 @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader)) {
                Song updatedSong = songService.updateAlbum(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedSong);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/isPrivate")
    public ResponseEntity<Song> updateSongLength(@Valid @RequestBody UpdateSongIsPrivateRequest request,
                                                 @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader)) {
                Song updatedSong = songService.updateIsPrivate(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedSong);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Song> deleteSong(@PathVariable("id") Integer id,
                                           @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(id, authHeader)) {
                songService.deleteSong(id);

                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    private boolean userIsAuthorizedToUpdate(int id, String authHeader) {
        Song songFromDb = songService.findSongById(id);
        int artistId = songFromDb.getArtist().getId();

        return artistId == tokenValidator.getIdFromAuthHeader(authHeader);
    }

    // This is needed for elegant error message when bean validation fails on registration
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleError(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return errors;
    }
}
