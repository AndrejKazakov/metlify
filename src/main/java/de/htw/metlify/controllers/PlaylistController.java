package de.htw.metlify.controllers;

import de.htw.metlify.models.Playlist;
import de.htw.metlify.models.Song;
import de.htw.metlify.models.User;
import de.htw.metlify.models.requests.updates.playlist.UpdatePlaylistGenreRequest;
import de.htw.metlify.models.requests.updates.playlist.UpdatePlaylistIsPrivateRequest;
import de.htw.metlify.models.requests.updates.playlist.UpdatePlaylistSongsRequest;
import de.htw.metlify.models.requests.updates.playlist.UpdatePlaylistTitleRequest;
import de.htw.metlify.services.playlist.PlaylistService;
import de.htw.metlify.services.song.SongService;
import de.htw.metlify.services.user.UserService;
import de.htw.metlify.utils.JwtTokenValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/playlists")
public class PlaylistController {

    private final PlaylistService playlistService;
    private final UserService userService;
    private final SongService songService;
    private final JwtTokenValidator tokenValidator;

    @Autowired
    public PlaylistController(PlaylistService playlistService, UserService userService, SongService songService, JwtTokenValidator tokenValidator) {
        this.playlistService = playlistService;
        this.userService = userService;
        this.songService = songService;
        this.tokenValidator = tokenValidator;
    }

    @GetMapping
    public ResponseEntity<List<Playlist>> getAllPlaylists() {
        List<Playlist> playlists = playlistService.findAllPlaylists();
        if (playlists.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(playlists);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Playlist> getPlaylistById(@PathVariable("id") Integer id,
                                                    @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        Playlist playlist = playlistService.findPlaylistById(id);

        if (playlist == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else if (!playlist.isPrivate() || userIsAuthorized(playlist.getOwner().getId(), authHeader)) {
            return ResponseEntity.status(HttpStatus.OK).body(playlist);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    private boolean userIsAuthorized(int id, String authHeader) {
        return id == tokenValidator.getIdFromAuthHeader(authHeader);
    }



    @GetMapping("/getBy")
    public ResponseEntity<List<Playlist>> getSongsByParameter(@RequestParam(name = "parameter") String parameter, @RequestParam(name = "value") String value) {
        try {
            List<Playlist> playlists = playlistService.getPlaylistsByParameter(parameter, value);

            if (playlists.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            } else {
                return ResponseEntity.status(HttpStatus.OK).body(playlists);
            }
        } catch (InvalidDataAccessApiUsageException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping("/owner")
    public ResponseEntity<List<Playlist>> getSongsByOwner(@RequestParam(name = "owner") String owner) {
        User user = userService.findUserByUsername(owner);

        List<Playlist> playlists = playlistService.findPlaylistsByOwner(user);

        if (playlists.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(playlists);
        }
    }

    @GetMapping("/generate")
    public ResponseEntity<Playlist> generatePlaylistByGenre(@RequestParam(name = "genre") String genre) {
        List<Song> songs = songService.getRandomSongsByGenre(genre);
        if (!songs.isEmpty()) {
            Playlist playlist = new Playlist();
            playlist.setGenre(genre);
            playlist.setSongs(new HashSet<>(songs));
            playlist.setTitle(genre + " Playlist");

            return ResponseEntity.status(HttpStatus.OK).body(playlist);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PostMapping
    public ResponseEntity<Playlist> createPlaylist(@Valid @RequestBody Playlist playlist,
                                                   @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        User owner = userService.findUserById(tokenValidator.getIdFromAuthHeader(authHeader));
        playlist.setOwner(owner);
        Playlist addedPlaylist = playlistService.createPlaylist(playlist);

        return ResponseEntity.status(HttpStatus.OK).body(addedPlaylist);
    }

    @PutMapping("/title")
    public ResponseEntity<Playlist> updatePlaylistTitle(@Valid @RequestBody UpdatePlaylistTitleRequest request,
                                                        @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader)) {
                Playlist updatedPlaylist = playlistService.updateTitle(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedPlaylist);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/genre")
    public ResponseEntity<Playlist> updatePlaylistGenre(@Valid @RequestBody UpdatePlaylistGenreRequest request,
                                                        @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader)) {
                Playlist updatedPlaylist = playlistService.updateGenre(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedPlaylist);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }


    @PutMapping("/songs")
    public ResponseEntity<Playlist> updatePlaylistSongs(@Valid @RequestBody UpdatePlaylistSongsRequest request,
                                                        @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {

        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader) && songsAreInDatabase(request.getNewSongs())) {
                Set<Song> newSongs = songService.getAllSongsWithMultipleIds(request.getNewSongs());
                Playlist updatedPlaylist = playlistService.updateSongs(request.getId(), newSongs);

                return ResponseEntity.status(HttpStatus.OK).body(updatedPlaylist);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/isPrivate")
    public ResponseEntity<Playlist> updatePlaylistIsPrivate(@Valid @RequestBody UpdatePlaylistIsPrivateRequest request,
                                                        @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader)) {
                Playlist updatedPlaylist = playlistService.updateIsPrivate(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedPlaylist);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Playlist> deletePlaylist(@PathVariable("id") Integer id,
                                                   @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(id, authHeader)) {
                playlistService.deletePlaylist(id);

                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }


    private boolean songsAreInDatabase(Set<Integer> songIds) {
        return songService.areSongsInDatabase(songIds);
    }

    private boolean userIsAuthorizedToUpdate(int id, String authHeader) {
        Playlist playlist = playlistService.findPlaylistById(id);
        int artistId = playlist.getOwner().getId();

        return artistId == tokenValidator.getIdFromAuthHeader(authHeader);
    }


    // This is needed for elegant error message when bean validation fails on registration
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleError(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return errors;
    }
}
